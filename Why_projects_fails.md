<img src="SKTedtech_160.png" width="120"/>

 

<div style ="margin:50px;"></p>

# Hands-On Lab: Why Projects Fail

 


#### Estimated duration: 15 minutes

 

## Objective
<li>Identify the reasons for project failure</li>

 

## Prerequisites
  You will need to have Microsoft Word installed on your Desktop or have an account for Microsoft Office 365 Online. Refer to the <a href="https://sk2280913.gitlab.io/Project-Lifecycle-Information-Sharing-and-Risk-Management/labs/office365/sign-up-for-office365.md.html" target = "_blank">[Prerequisites]</a> for steps on how to create a Microsoft Office 365 Online account and access Microsoft Word Online.</p>
## Scenario: Inventory Management System Upgrade Project
Sunil is an operations manager. He has had little project management training. However, Sunil accepted the project manager role for an inventory management system upgrade project at his manager's request. The aim of this project was to move from an outdated inventory management software and several manual steps to a new automated system. Sunil’s objectives for the upgrade project were to manage the software upgrade and develop automated solutions to eliminate manual steps. The team completed the work but had a three-week delay in schedule and ran 25% over budget.</p>
At project start, Ram, Sunil’s manager, had asked Sunil to do his best on the project. However, this work was over and above his daily job in operations. Ram was the project Sponsor. Based on time constraints, Ram and Sunil agreed to forego the Project Charter process and use their conversation as the basis to move the project forward.</p>
Ram was not exactly sure about the time and cost expectations for the system upgrade when Sunil was assigned. Ram had made a commitment to management to complete the project in two months. He also estimated that the project would not exceed $25k. He based this estimate on the best information he had available at the time. But the estimate Ram used was not data based. It was his gut instinct. Ram stated he would work with Sunil as the project moved forward to try to determine exact requirements. Ram consistently shared new information with Sunil as the project entered the planning and execution phases.</p>
Sunil initially tried to perform all project tasks by himself. He soon realized that this project was far more than he could handle on his own. In addition, Sunil soon realized that he needed Subject Matter Expert support. Ram did his best to find other resources to help Sunil as the project progressed.</p>
Priya and Alex were assigned to the project two weeks after it started. Alex was an innovator. He worked tirelessly to develop new automated solutions and would reveal his work to the rest of the team periodically. Priya was a process manager. It took some time, but she was able to address some issues that caught the team by surprise that were not anticipated at the beginning of the project.</p>
Halfway through the project, Alex stumbled upon an alternative software solution that was better than what was originally planned. The software was more expensive but provided far more functionality than the original software that had been planned. Priya and Alex worked hard to develop solutions. They acquired the software, updated the system, and automated many prior manual steps in the process.</p>
When the final solution was provided, Ram had some questions and prophereosed some modifications. In the end, the project was completed. But it was completed behind schedule, and the proposed budget for the project was exceeded.</p></p>
## Exercise 
Identify at least five reasons for project failure in the above scenario</p>
<b>Step 1:</b> Click [here](Reasons%20for%20Project%20Failure%20Worksheet.docx) to download the <b>Potential Reasons</b> worksheet.</p>
<b>Step 2:</b> Add the reasons. Use one row for each reason.</p>
<b>Step 3:</b> Save the file.</p></p>

 

<b>Make sure to attempt the exercise as it will help you firm up the concepts covered in the module.</b></p></p></p>

 

## Potential Solution
After completing the exercise, refer to the [Potential Solution](Reasons%20for%20Project%20Failure_Potential%20Solutions.pdf) document to review potential reasons for project failure in the given scenario.
</div>
